### imports

from deployment import *
from os.path import exists, join
from datetime import datetime
ENV = 'stag'
Commit_Time_N_Hash = namedtuple("Commit_Time_H_Hash", ["time", "hash"])
dhash = "#"

env.user = 'ali'
env.hosts = ['ali-ThinkPad-E570']
env.password = "123123"

### global variables
last_deployment_info_file = 'last_commit_info.txt'


### sub functions


def check_repo_for_hash():
    print("check_repo_for_hash: starting the hash check on last 3 commits")

    last_deployment_commit_info = get_last_commit_time_and_hash()
    print(f"check_repo_for_hash: Commit_Time_N_Hash is passed and it is giving us time and hash as {last_deployment_commit_info}")

    for i in range(1,4):
        log_commit_number = str(i)

        commit_time = local(f'git log -{log_commit_number} --pretty="%at" | tail -n 1', capture=True)
        commit_time = int(commit_time)
        print(f"check_repo_for_hash: commit time for {log_commit_number} is: {commit_time}")
        commit_message = local(f'git log -{log_commit_number} --pretty="%s" | tail -n 1', capture=True)
        print(f"check_repo_for_hash: commit message for {log_commit_number} is: {commit_message}")

        if commit_message.startswith(dhash):
            print("check_repo_for_hash: commit starts with a # found")

            if not last_deployment_commit_info:
                print("check_repo_for_hash: No last commit file found, hence going to deploy now")
                return True
            else:
                print("check_repo_for_hash: Last deployment file found. Hence, checking the commit time if "
                             "it is the same as of the current dhash")
                if commit_time > last_deployment_commit_info.time:
                    print(f"check_repo_for_hash: Current commit time: {commit_time} is greater than last commit time: {last_deployment_commit_info.time}"
                                 f"Therefore, deploying now")
                    return True
                else:
                    print(f"check_repo_for_hash: Current commit time: {commit_time} is not greater than the last commit time: {last_deployment_commit_info.time}"
                                 f". Therefore, returning False")
                    return  False
        else:
            print(f"check_repo_for_hash: Commit message does not start with a dhash for commit number: {log_commit_number}"
                         f"Therefore giving a pass")
            pass
    return False




def get_git_cmd_for_changed_files():

    last_deployment_commit_info = get_last_commit_time_and_hash()
    print(f"get_git_cmd_for_changed_files: Getting the last commit time and hash")
    if last_deployment_commit_info:
        commit_hash = local('git log -1 --pretty=%h', capture=True)
        print(f"get_git_cmd_for_changed_files: commit_hash: {commit_hash}")
        # This git command handles file name changes as well
        # need to escape = for fab command          # {latest commit} {old commit}
        git_cmd = f'git diff --pretty\="" --name-only {commit_hash} {last_deployment_commit_info.hash}' #gives the file names after comparing the latest commit with previous commit
        print(f"get_git_cmd_for_changed_files: git cmd: {git_cmd}")
    else:
        # Return all the files that are committed & tracked by git
        print(f"get_git_cmd_for_changed_files: No last_deployment_commit_info found")
        git_cmd = f'git ls-tree --full-tree -r --name-only HEAD'  # f'git log --pretty\="" --name-only'
        print(f"get_git_cmd_for_changed_files: git_cmd: {git_cmd}")
    return git_cmd

def prepare_normal_deployment(git_cmd):
    print('Starting Normal Deployment')
    local(f'fab deployment:environment=dev,git_cmd_changed_files=\'{git_cmd}\'')
    # local(f'fab testing_gocd_ability')


### main function

def main():
    if check_repo_for_hash():
        git_cmd = get_git_cmd_for_changed_files()

        print(f"main: git_cmd {git_cmd} received, now going for Normal Deployment")

        prepare_normal_deployment(git_cmd)


### start

if __name__ == '__main__':
    main()

