import Config

config :pg_configs,
  adapter: Ecto.Adapters.Postgres,
  username: "qa_se_user",
  password: "se+03+18+04",
  database: "se_qa_db_ex",
  hostname: "qa-se-cloud.cob7qiubxdpv.us-east-1.rds.amazonaws.com",
  pool_size: 10

config :web_configs,
  version: "6.0.12.RC1.d569714",
  web_version: "6.0.13.RC1.f363846f",
  app_base_url: "https://sapp.skyelectric.com",
  system_ids_for_dummy_predictions: ["fa576b26-0bd8-4ea3-b035-d12310f472e8", "9ee70f0a-abed-4af9-a27c-1d1d57680f62"]

config :api_configs,
  host: "sapi.skyelectric.com",
  port: 4000,
  server: true,
  secret_key_base: "7GLIB//1sDPMn6w2nKORwefhg8k81KIPLYGybWh54iVHa8MY9rsIBFOEH43+93dO",
  secret_key: "snF4JNR/C6iLNtkO5S2nFuFlFaJYOD6CA5O1qrJl0NQ0yVVRSQE5aM7M+mt7OuHp"

config :smtp_configs,
  relay: "email-smtp.us-east-1.amazonaws.com",
  username: "AKIAI72DBWB2BEJE34OQ",
  password: "AsPyxe/qEyUIwvqRQVAyrtOoSrPwnw8ngzYWtB8zCD+0",
  #username: "AKIAWZOXZCJDDUN5HFUH",
  #password: "GdHkpY6UYBRwSWtseyqtynKDjIfbDbn5xdV+RkCW",
  sender_email: "customer.care@skyelectric.com",
  tls: :always,
  auth: :always,
  port: 587,
  retries: 2

config :scylla_configs,
  pool_size: 10,
  nodes: ["10.0.10.196:9042", "10.0.2.207:9042", "10.0.4.169:9042"],
  #nodes: ["10.0.2.98:9042"],
  keyspace: "se_stats_dev_v3",
  username: "se_user",
  password: "se@123++CS"

config :es_configs,
  url: "https://vpc-sanalytics-vm42zpudjdqdjywvwtjtyc2pva.us-east-1.es.amazonaws.com/"

config :s3_configs,
  access_key: "AKIAWZOXZCJDH6A27C5K",
  secret_key: "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",
  region: "us-east-1",
  system_bills_bucket: "se-staging-system-bills",
  system_docs_bucket: "se-staging-system-docs",
  system_logs_bucket: "se-staging-ngm-logs",
  energy_reports_bucket: "se-staging-energy-reports",
  s3_software_download_key: "AKIAWZOXZCJDG5XME3P7",
  s3_software_download_secret: "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",
  s3_software_updates_bucket: "se-builds",
  s3_software_releases_bucket: "se-staging-software-releases",
  s3_bill_uploads_bucket: "se-staging-bill-uploads"

config :apns_configs,
  cert_path: "/opt/skyelectric/certs/prod/cert.pem",
  key_path: "/opt/skyelectric/certs/prod/key_unencrypted.pem",
  apns_2197: true,
  mode: :prod

config :fcm_configs,
  key: "AAAAFUF9W0Y:APA91bHkoj3yCnCTVKEcxe2VRkGu9Q1VTXm8wQTCTiZMqYJTacdpmgXnlZibXO8Rg9jUuzc1WSw6_IL7bwbtXUnKYmXWbwAOX2-6ih9gTs8kGxn6W0UJeZZgBVTQywc0QOKX3_Emng2Y"

config :dbm_configs,
  host: "sdbm.skyelectric.com",
  port: 443

config :google_auth_configs,
  disable_auth_restriction: true

config :sma_configs,
  username: "noc.sma@skyelectric.com",
  password: "uqR6Hn!+d*HjwTd",
  app_name: "SPAndroidApp-351049796",
  app_id: "fe6ccd63-a38a-4fd4-98a5-4eecbc95d28e"

config :huawei_configs,
  username: "Mushtaq_API",
  system_code: "Huawei@07_20",
  base_url: "https://13.251.20.171/thirdData/"

config :survey_configs,
  survey_required_deployment_date: ~N[2020-11-19 00:00:00]
