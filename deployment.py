import os
from fabric.api import *
from collections import namedtuple
from os.path import exists, join
from datetime import datetime
now = datetime.now()
date = now.strftime(("%Y%m%d_%H"))
ENV = 'stag'  #stag or prod
Commit_Time_N_Hash = namedtuple("Commit_Time_H_Hash", ["time", "hash"])
dhash = "#"
last_deployment_info_file = '/opt/skyelectric/se_gocd/last_commit_info.txt'

# from main import get_last_commit_time_and_hash
def get_last_commit_time_and_hash():
    """"If the file of last deployed commit exist, it provides with the time and hash
    else
    return False"""

    global last_deployment_info_file
    print("get_last_commit_time_and_hash: global variable called. Now going to read the file")
    if os.path.exists(last_deployment_info_file):
        print("get_last_commit_time_and_hash: last_deployment_info_file exists, now reading it")
        with open(last_deployment_info_file, 'r') as file:
            commit_info = file.readlines()
            print("get_last_commit_time_and_hash: last commit file is read and now extracting the time and hash")
            print(f"get_last_commit_time_and_hash: commit_info: {commit_info}")
            return Commit_Time_N_Hash(time=int(commit_info[0]), hash=commit_info[1].strip()) #Commit_Time_N_Hash defined after the imports

    else:
        print("get_last_commit_time_and_hash: no file found, hence returning False")
        return False



def normal_deployment(git_cmd):
    print(f"normal_deployment: Starting Normal deployment")
    #Deployment server: Taking backup
    with cd('/opt/skyelectric'):
        run(f' mkdir -p backup/api_{date}')
        run(f'cp -rf se_cloud/api backup/api_{date}')
    with lcd('/opt/skyelectric/se_cloud'):
        # run('git checkout')
        # local('git checkout')
        config_file = f'.env/.{ENV}.env'
        config_file_name = f'.{ENV}.env'
        # config_file = 'configs_stag.exs'
        # config_file_location = join(config_folder, config_file)
        local(f'cp {config_file} backup')
#hello to the world
        pre_last_commit_hash = local(f'git log -1 --pretty=%h', capture=True)
        last_commit_hash = pre_last_commit_hash.rstrip("\n")
        print('last commit hash:' + last_commit_hash)
        last_commit_time = local(f'git log {last_commit_hash} --pretty=%at | head -n 1', capture=True)
        print('last commit time:' + last_commit_time)
        last_deployment_commit_info = get_last_commit_time_and_hash()
        backed_up_config_file = f'backup/{config_file_name}'
        print(last_deployment_commit_info)

        local(f'sed s/{last_deployment_commit_info.hash}/{last_commit_hash}/ < {backed_up_config_file} > {config_file}')

        ##SAVING LAST COMMIT INFO
        local(f'git log {last_commit_hash} --pretty=%at | head -n 1 > {last_deployment_info_file}; '
              f'git log -1 --pretty=%h >> {last_deployment_info_file}')

        local(f'MIX_ENV={ENV} mix deps.get')
        ##RELEASE BINARY
        local(f'MIX_ENV={ENV} mix release api')


        with lcd(f'_build/{ENV}/rel'):
            local(f'zip -r api.zip api')
            run(f'mkdir -p /opt/skyelectric/builds/{date}')
            put(f'api.zip', f'/opt/skyelectric/builds/{date}', use_glob=True)

        with cd(f'/opt/skyelectric/builds/{date}'):
            run(f'unzip api.zip')
        with cd(f'/opt/skyelectric/se_cloud'):
            run(f'se_cloud.sh stop')
            run(f'cp -Rf /opt/skyelectric/builds/{date}/api /opt/skyelectric/se_cloud/api')
            run(f'se_cloud.sh start')
            run('sleep 5')
            #verfiy port 4000 is active
            port_active = run('lsof -i:4000')
            if port_active:
                print('Port 4000 is active')
            else:
                print("Port 4000 is inactive") #this msg can be ignored for now, for now it shows SUCCESS

