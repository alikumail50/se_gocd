from fabric.api import *
# import pandas as pd
from deployment import normal_deployment
# from fab_utils import ext_properties
env.user = 'ali'
env.hosts = ['ali-ThinkPad-E570']
env.password = "123123"

ext_properties = {}
def _get_instances(environment='dev', role='simu'):
    print('fabfile.get_instances: Get list of servers for environment: ' +
              str(environment) + ' and role: ' + str(role))
    instances = []

    if environment == "dev":
        print("fabfile.get_instances: Adding dev host to " + role)
        instances.append("localhost")

    if environment == "qa":
        print("fabfile.get_instances: Adding qa host to " + role)
        instances.append("192.168.3.44")

    if environment == "prod":
        print("fabfile.get_instances: Adding prod host to " +
                  role + " which is currently dev server.")
        instances.append("172.0.12.139")

    if environment == "local":
        print("fabfile.get_instances() Adding local host to " +
                  role + " which is currently dev server.")
        instances.append("192.168.3.44")

    return instances

def _set_hosts(environment='dev'):
    env.roledefs = {"simu": []}  # {'simu': [], 'app': []}

    ext_properties['stack_environment'] = environment

    simu_servers = _get_instances(environment, 'simu')
    env.roledefs['simu'] = simu_servers        # TODO fix return thingi, like append or extend

    # Check if list of servers was retrieved to upload the files
    if len(env.roledefs) == 0:
        print("fabfile.set_hosts() Exiting...")
        exit(1)
    # else:
    #     for instance in instances:
    #         env.hosts.append(instance['IP'])
    print(f'fabfile.set_hosts() + {env.roledefs}' )


def deployment(environment='dev', git_cmd_changed_files=''):
    print("Executing deployment")
    # _set_hosts(environment)
    execute(normal_deployment, git_cmd_changed_files)

















# def update():
#     with cd('/opt/skyelectric'):
#         run('mkdir new_folder')
#         with cd('new_folder'):
#             run('touch made_it.txt')
#
# def make_dir():
#     with cd ('/'):
#         run('ls -altr >> somefile.txt')
#         run ('pwd >> somefile.txt')
#         run ('cp somefile.txt temporary/')
#         run('touch someotherfile.txt')
#         run(' echo "hello world goCD is working fineeee"')
#
# def testing_gocd_ability():
#     execute(make_file)
#
